#include <iostream>
#include "clockType.h"

using namespace std;

int main()
{
    cout << "Hello world!" << endl;
    clockType t;
    t.setTime(10, 0, 0);
    clockType t1;
    t1.setTime(10, 0, 1);

    t.incrementSeconds();
    if(t.compare(t1)){
      cout << "Times are equal" << endl;
      t1.printTime();
      t.printTime();
    } else {
      cout << "Times are not equal" << endl;
      t.printTime();
      t1.printTime();
    }

    return 0;
}
