#include "clockType.h"
#include <iostream>

using namespace std;

clockType::clockType()
{
    //ctor
    hours = 0;
    minutes = 0;
    seconds = 0;
}

void clockType::setTime(int h, int m, int s){

   if(h >= 0 && h <= 23)
    hours = h;

   if(m >= 0 && m <= 59)
    minutes = m;

   if(s >= 0 && s <= 59)
    seconds = s;

}

void clockType::returnTime(int &h, int &m, int &s){
   h = hours;
   m = minutes;
   s = seconds;
}

void clockType::printTime(){

  cout << "Time is : " << hours << ":" << minutes << ":" << seconds << endl;

}

void clockType::incrementSeconds(){
   if(seconds < 59){
     seconds ++;
   } else {
     seconds = 0;
     incrementMinutes();
   }
}


void clockType::incrementMinutes(){
   if(minutes < 59){
     minutes ++;
   } else {
     minutes = 0;
     incrementHours();
   }
}

void clockType::incrementHours(){
   if(hours < 23){
     hours ++;
   } else {
     hours = 0;
   }
}

int clockType::compare(const clockType &t){

   if(t.hours == hours && t.minutes == minutes && t.seconds == seconds )
     return 1;

   return 0;
}

clockType::~clockType()
{
    //dtor
}
