#ifndef CLOCKTYPE_H
#define CLOCKTYPE_H


class clockType
{
    public:
        clockType();
        virtual ~clockType();
        void setTime(int h, int m, int s);
        void returnTime(int &h, int &m, int &s);
        void printTime();
        void incrementSeconds();
        void incrementMinutes();
        void incrementHours();
        int compare(const clockType &t);

    private:
        int hours;
        int minutes;
        int seconds;
};

#endif // CLOCKTYPE_H
